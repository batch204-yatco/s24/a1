// console.log("Hello World");

const num = 2
const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

const address = ["258 Washington Ave NW", "California", "90011"];
const [street, state, zipcode] = address;
console.log(`I live at ${street}, ${state}, ${zipcode}`);

const animal = {
		name: "Lolong",
		specie: "saltwater crocodile",
		weightInKg: 1075,
		measurementInFeet: 20,
		remMeasurementInInches: 3,
};

const {name, specie, weightInKg, measurementInFeet, remMeasurementInInches} = animal;
console.log(`${name} was a ${specie}. He weighed at ${weightInKg} kgs with a measurement of ${measurementInFeet} ft ${remMeasurementInInches} in.`)

// const numbers = [1, 2, 3, 4, 5];
// numbers.forEach((number) => console.log(parseInt(`${number}`)));
	
	// const numbers = [1, 2, 3, 4, 5];

	// numbers.forEach((number) => {
	// 	function funcName(returnNum){
	// 		console.log(returnNum);
	// 		return returnNum;
	// }
	
	// let eachNum = funcName(numbers);
	// console.log(eachNum);	
	// });

let numbers = [1, 2, 3, 4, 5];

const invokeFunc = () => {
	numbers.forEach((number) => {
		console.log(parseInt(`${number}`));
		return number;
	});
}

invokeFunc();

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myNewDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myNewDog);